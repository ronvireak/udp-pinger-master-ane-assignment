import socket

ip4 = "127.0.0.1"  # Local IP address to bind to
ip6 = "::1"  #IPv6
port = 5000  # Port number to bind to

sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)  # Create a UDP socket with IPv6
sock.bind((ip6, port))  # Bind the socket to the IP address and port

print(f'Start listening to {ip6}:{port}')  # Print the listening status

while True:  # Run indefinitely to listen for messages
    data, addr = sock.recvfrom(1024)  # Receive data from the socket (buffer size 1024 bytes)
    print(f"received message: {data}")  # Print the received message
