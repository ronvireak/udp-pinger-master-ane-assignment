import socket

ip4 = "127.0.0.1"  # Target IPv4 address
ip6 = "::1"  #IPv6
port = 5000  # Target port number
msg = b"MITE18 Evening Advance Network Engineering Using IPv6"  # Message to be sent (bytes)

print(f'Sending {msg} to {ip6}:{port}')  # Print the message and destination

sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)  # Create a UDP socket with IPv6
sock.sendto(msg, (ip6, port))  # Send the message to the specified IP and port
